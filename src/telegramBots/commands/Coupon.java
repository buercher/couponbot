package telegramBots.commands;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.DeleteMessage;
import com.pengrad.telegrambot.request.SendDocument;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.BaseResponse;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Time;
import java.util.*;
import java.util.stream.Stream;

import static telegramBots.TelegramBotForCouponBot.*;

public class Coupon {
    private Coupon() {
    }

    public static void command(Message message) throws IOException {
        File directory = new File("ressources");
        List<String> AvailableCoupons = new ArrayList<>();
        Map<Integer, Integer> couponMap = new HashMap<>();
        try (Stream<Path> files = Files.list(directory.toPath())) {
            files.filter(Files::isDirectory).forEach(file -> {
                try (Stream<Path> filesInDirectory = Files.list(file)) {
                    AvailableCoupons.add(file.getFileName().toString());
                    couponMap.put(AvailableCoupons.size() - 1, (int) filesInDirectory.count());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
            SendMessage request;
            if (Method.isDMRestricted(message)) {
                DeleteMessage deleteMessage = new DeleteMessage(message.chat().id(), message.messageId());
                request = new SendMessage(message.from().id(),
                        messageStartText(message.from().languageCode(), AvailableCoupons, couponMap));
                bot.execute(deleteMessage);
            } else {
                request = new SendMessage(message.chat().id(),
                        messageStartText(message.from().languageCode(), AvailableCoupons, couponMap));
                request.messageThreadId(message.messageThreadId() == null ? 0 : message.messageThreadId());
            }
            request.parseMode(ParseMode.Markdown);
            request.disableNotification(true).disableWebPagePreview(true);
            bot.execute(request);
        }
    }

    /**
     * Gives the message start text in the correct language
     *
     * @param languageCode the language code of the user (fr or else)
     * @return the message start text in the correct language
     */
    private static String messageStartText(String languageCode,
                                           List<String> AvailableCoupons,
                                           Map<Integer, Integer> couponMap) {
        TreeMap<String, Integer> treeMap = new TreeMap<>();
        couponMap.keySet().forEach(key -> treeMap.put(AvailableCoupons.get(key), couponMap.get(key)));
        StringBuilder messageStartText = new StringBuilder();
        if ("fr".equals(languageCode)) {
            messageStartText.append("Voici la liste des coupons : ");
        } else {
            messageStartText.append("Here's a list of the coupons :");
        }
        int count = 0;
        for (String coupon : treeMap.keySet()) {
            messageStartText.append("\n*").append(count);
            if (count < 10) {
                messageStartText.append(" ");
            }
            messageStartText.append("*: (").append(treeMap.get(coupon)).append(" coupon) ").append(coupon);
            count++;
        }
        return messageStartText.toString();
    }

    public static void giveCoupon(Message message, int couponNumber) {
        long chatID = message.chat().id();
        if (Method.isDMRestricted(message)) {
            DeleteMessage deleteMessage = new DeleteMessage(message.chat().id(), message.messageId());
            bot.execute(deleteMessage);
            chatID = message.from().id();
            if (!couponUsage.containsKey(message.from().id())) {
                couponUsage.put(message.from().id(), new ArrayList<>());
            }
            couponUsage.get(message.from().id()).add(new Date());
            couponUsage.get(message.from().id()).add(new Date());
            couponUsage.get(message.from().id()).add(new Date());
        }
        if (couponNumber < 0 || couponNumber >= COUPONS.size()) {
            SendMessage request;
            if ("fr".equals(message.from().languageCode())) {
                request = new SendMessage(chatID, "Invalid coupon number");
            } else {
                request = new SendMessage(chatID, "Nombre invalide de coupon");
            }
            if (!Method.isDMRestricted(message)) {
                request.replyToMessageId(message.messageId())
                        .messageThreadId(message.messageThreadId() == null ? 0 : message.messageThreadId());
            }
            request.disableNotification(true).disableWebPagePreview(true);
            bot.execute(request);
        } else {
            if (!couponUsage.containsKey(message.from().id())) {
                couponUsage.put(message.from().id(), new ArrayList<>());
            }
            List<Date> userUsage = couponUsage.get(message.from().id())
                    .stream()
                    .filter(date -> date.getTime() < new Date().getTime() - 24 * 3600 * 1000).toList();
            couponUsage.get(message.from().id()).removeAll(userUsage);
            if (!couponUsage.get(message.from().id()).isEmpty()
                    && !(message.from().id() == 2058577875)
                    && !(message.from().id() == 1783734583)) {

                SendMessage request;
                if ("fr".equals(message.from().languageCode())) {
                    request = new SendMessage(chatID,
                            "Seulement un coupons par jour, réessayez plus tard dans " +
                                    new Time(23 * 3600 * 1000 -
                                            new Date().getTime() +
                                            couponUsage.get(message.from().id()).get(0).getTime()));
                } else {
                    request = new SendMessage(chatID,
                            "Only one Coupons per day, try again later in " +
                                    new Time(23 * 3600 * 1000 -
                                            new Date().getTime() +
                                            couponUsage.get(message.from().id()).get(0).getTime()));
                }
                if (!Method.isDMRestricted(message)) {
                    request.replyToMessageId(message.messageId())
                            .messageThreadId(message.messageThreadId() == null ? 0 : message.messageThreadId());
                }
                request.disableNotification(true).disableWebPagePreview(true);
                bot.execute(request);
            } else {
                File directory = new File("ressources/" + COUPONS.get(couponNumber));
                try (Stream<Path> files = Files.list(directory.toPath())) {
                    Optional<Path> path = files.findFirst();
                    if (path.isEmpty()) {
                        SendMessage request;
                        if ("fr".equals(message.from().languageCode())) {
                            request = new SendMessage(chatID,
                                    "No more coupon available DM @nico_1453");
                        } else {
                            request = new SendMessage(chatID,
                                    "Plus de coupon disponible DM @nico_1453");
                        }
                        if (!Method.isDMRestricted(message)) {
                            request.replyToMessageId(message.messageId())
                                    .messageThreadId(message.messageThreadId() == null ? 0 : message.messageThreadId());
                        }
                        request.disableNotification(true).disableWebPagePreview(true);
                        bot.execute(request);
                    } else {
                        couponUsage.get(message.from().id()).add(new Date());
                        File file = new File(path.get().toString());
                        String MessageResponseText;
                        if ("fr".equals(message.from().languageCode())) {
                            MessageResponseText = "Voici votre coupon " + COUPONS.get(couponNumber)
                                    + " (l'email utilisé pour le coupons est dans le nom du fichier)";
                        } else {
                            MessageResponseText = "Here is your coupon : " + COUPONS.get(couponNumber)
                                    + " (the email used for the coupon is in the file name)";
                        }
                        SendDocument fileResponse = new SendDocument(chatID, file)
                                .caption(MessageResponseText)
                                .disableNotification(true);
                        if (!Method.isDMRestricted(message)) {
                            fileResponse.replyToMessageId(message.messageId())
                                    .messageThreadId(message.messageThreadId() == null ? 0 : message.messageThreadId());
                        }
                        BaseResponse response = bot.execute(fileResponse);
                        boolean isDeleted = false;
                        if (!(response.errorCode() == 403)) {
                            isDeleted = Files.deleteIfExists(path.get());
                        }
                        String user;
                        if (message.from().username() == null) {
                            user = "'" + message.from().firstName() + " " + message.from().lastName() + "'";
                        } else {
                            user = "@" + message.from().username();
                        }
                        System.out.print("User " + user + " got coupon \"" +
                                COUPONS.get(couponNumber) + "\", (" + new Date() + ")");
                        System.out.println(", Files Deleted: (" + isDeleted + ")," +
                                " Email: (" + file.getName() + ")");
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
