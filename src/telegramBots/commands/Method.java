package telegramBots.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pengrad.telegrambot.model.Message;

import java.io.IOException;
import java.util.Objects;
import java.util.Set;

import static telegramBots.TelegramBotForCouponBot.adminChannelSetup;
import static telegramBots.TelegramBotForCouponBot.adminChannelSetupFile;

public class Method {
    private Method() {
    }

    public static void updateAdminSetupFile() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(adminChannelSetupFile, adminChannelSetup);
        } catch (IOException e) {
            e.fillInStackTrace();
        }
    }

    public static Set<Integer> permCheck(Message message, String perm) {
        if (adminChannelSetup.containsKey(message.chat().id())) {
            if (adminChannelSetup.get(message.chat().id()).containsKey(perm)) {
                return adminChannelSetup.get(message.chat().id()).get(perm);
            }
        }
        return null;
    }

    public static boolean isThreadRestricted(Message message) {
        Set<Integer> adminPerms = permCheck(message, "RestrictThread");
        if (Objects.nonNull(adminPerms)) {
            return adminPerms.contains(message.messageThreadId());
        }
        return true;
    }

    public static boolean isDMRestricted(Message message) {
        Set<Integer> adminPerms = permCheck(message, "RestrictToDM");
        return Objects.nonNull(adminPerms);
    }
}
