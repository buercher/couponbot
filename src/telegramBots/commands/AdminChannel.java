package telegramBots.commands;

import com.pengrad.telegrambot.model.Chat;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.request.GetChat;
import com.pengrad.telegrambot.request.GetChatAdministrators;
import com.pengrad.telegrambot.request.SendMessage;

import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

import static telegramBots.TelegramBotForCouponBot.adminChannelSetup;
import static telegramBots.TelegramBotForCouponBot.bot;

public class AdminChannel {
    private AdminChannel() {
    }

    public static void addBotToThread(Message message) {
        Chat.Type chatType = bot.execute(
                new GetChat(message.chat().id())).chat().type();
        if (chatType.equals(Chat.Type.group) || chatType.equals(Chat.Type.supergroup)) {
            if (bot.execute(
                            new GetChatAdministrators(message.chat().id())).administrators()
                    .stream().anyMatch(l -> l.user().id().equals(message.from().id()))) {
                if (adminChannelSetup.containsKey(message.chat().id())) {
                    if (adminChannelSetup
                            .get(message.chat().id())
                            .containsKey("RestrictThread")) {
                        adminChannelSetup
                                .get(message.chat().id())
                                .get("RestrictThread")
                                .add(message.messageThreadId());
                    } else {
                        adminChannelSetup
                                .get(message.chat().id())
                                .put("RestrictThread", Collections.singleton(message.messageThreadId()));
                    }
                } else {
                    HashMap<String, Set<Integer>> map = new HashMap<>();
                    map.put("RestrictThread", Collections.singleton(message.messageThreadId()));
                    adminChannelSetup.put(message.chat().id(), map);
                }
                SendMessage request = new SendMessage(message.chat().id(), "Chat Added").disableNotification(true)
                        .messageThreadId(message.messageThreadId() == null ? 0 : message.messageThreadId())
                        .replyToMessageId(message.messageId());
                Method.updateAdminSetupFile();
                bot.execute(request);
            }
        }
    }

    public static void removeBotFromThread(Message message) {
        Chat.Type chatType = bot.execute(
                new GetChat(message.chat().id())).chat().type();
        if (chatType.equals(Chat.Type.group) || chatType.equals(Chat.Type.supergroup)) {
            if (bot.execute(
                            new GetChatAdministrators(message.chat().id())).administrators()
                    .stream().anyMatch(l -> l.user().id().equals(message.from().id()))) {
                if (adminChannelSetup.containsKey(message.chat().id())) {
                    if (adminChannelSetup
                            .get(message.chat().id())
                            .containsKey("RestrictThread")) {

                        adminChannelSetup
                                .get(message.chat().id())
                                .get("RestrictThread")
                                .remove(message.messageThreadId());
                    }
                } else {
                    HashMap<String, Set<Integer>> map = new HashMap<>();
                    map.put("RestrictThread", Collections.emptySet());
                    adminChannelSetup.put(message.chat().id(), map);
                }
                SendMessage request = new SendMessage(message.chat().id(), "Chat Removed").disableNotification(true)
                        .messageThreadId(message.messageThreadId() == null ? 0 : message.messageThreadId())
                        .replyToMessageId(message.messageId());
                Method.updateAdminSetupFile();
                bot.execute(request);
            }
        }
    }

    public static void onlyDM(Message message) {
        Chat.Type chatType = bot.execute(
                new GetChat(message.chat().id())).chat().type();
        if (chatType.equals(Chat.Type.group) || chatType.equals(Chat.Type.supergroup)) {
            if (bot.execute(
                            new GetChatAdministrators(message.chat().id())).administrators()
                    .stream().anyMatch(l -> l.user().id().equals(message.from().id()))) {
                String response;
                if (adminChannelSetup.containsKey(message.chat().id())) {
                    if (adminChannelSetup
                            .get(message.chat().id())
                            .containsKey("RestrictToDM")) {
                        adminChannelSetup.get(message.chat().id()).remove("RestrictToDM");
                        response = "Restriction removed";
                    } else {
                        adminChannelSetup
                                .get(message.chat().id())
                                .put("RestrictToDM", Collections.emptySet());
                        response = "Restriction added";
                    }
                } else {
                    HashMap<String, Set<Integer>> map = new HashMap<>();
                    map.put("RestrictToDM", Collections.emptySet());
                    adminChannelSetup.put(message.chat().id(), map);
                    response = "Restriction added";
                }
                SendMessage request = new SendMessage(message.chat().id(), response).disableNotification(true)
                        .messageThreadId(message.messageThreadId() == null ? 0 : message.messageThreadId())
                        .replyToMessageId(message.messageId());
                Method.updateAdminSetupFile();
                bot.execute(request);
            }
        } else {
            SendMessage request = new SendMessage(message.chat().id(), "This is not a Group")
                    .disableNotification(true)
                    .messageThreadId(message.messageThreadId() == null ? 0 : message.messageThreadId())
                    .replyToMessageId(message.messageId());
            Method.updateAdminSetupFile();
            bot.execute(request);
        }
    }
}
