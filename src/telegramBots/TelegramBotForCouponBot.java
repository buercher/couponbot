package telegramBots;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import telegramBots.commands.AdminChannel;
import telegramBots.commands.Coupon;
import telegramBots.commands.Method;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

public class TelegramBotForCouponBot {

    public static final Map<Long, List<Date>> couponUsage = new HashMap<>();

    public static final List<String> COUPONS = new ArrayList<>();
    public static final File adminChannelSetupFile = new File("database/adminChannelSetup.json");

    public static HashMap<Long, HashMap<String, Set<Integer>>> adminChannelSetup;
    public static TelegramBot bot;

    private TelegramBotForCouponBot() {
    }

    public static void main(String... args) throws IOException {
        bot = new TelegramBot(args[0]);
        File directory = new File("ressources");
        if (!directory.exists() && !directory.mkdirs()) {
            throw new IOException("Failed to create folder '" + directory.getPath() + "'");
        }

        File database = new File("database");
        if (!database.exists() && !database.mkdirs()) {
            throw new IOException("Failed to create folder '" + database.getPath() + "'");
        }

        if (Files.exists(adminChannelSetupFile.toPath())) {
            String jsonString = Files.readString(adminChannelSetupFile.toPath());
            TypeReference<HashMap<Long, HashMap<String, Set<Integer>>>> typeRef = new TypeReference<>() {
            };
            adminChannelSetup = new ObjectMapper().readValue(jsonString, typeRef);
        } else {
            adminChannelSetup = new HashMap<>();
        }

        try (Stream<Path> files = Files.list(directory.toPath())) {
            COUPONS.addAll(files.filter(Files::isDirectory).map(Path::getFileName).map(Path::toString).toList());
        }
        COUPONS.sort(String::compareTo);
        // Set up a listener for updates from the Telegram Bot API.
        // Processes each update and performs actions based on the type of update.
        bot.setUpdatesListener(updates -> {
            for (Update update : updates) {
                // Check if the update has a message
                if (update.message() != null) {
                    Message message = update.message();
                    if (Objects.nonNull(message.text())) {
                        String receivedText = message.text()
                                .replaceAll("@CoopCouponBot", "")
                                .toLowerCase();
                        switch (receivedText) {
                            case "/coupon" -> {
                                try {
                                    if (Method.isThreadRestricted(message)) {
                                        Coupon.command(message);
                                    }
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                            case "/addbottotopic" -> AdminChannel.addBotToThread(message);
                            case "/removebotfromtopic" -> AdminChannel.removeBotFromThread(message);
                            case "/restrictdm" -> AdminChannel.onlyDM(message);
                            default -> {
                                if (Method.isThreadRestricted(message)) {
                                    if (receivedText.matches("[0-9]+")) {
                                        try {
                                            int number = Integer.parseInt(receivedText);
                                            Coupon.giveCoupon(message, number);
                                        } catch (NumberFormatException ignored) {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });
    }
}
